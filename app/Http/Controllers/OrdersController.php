<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Order;
use App\OrderDetail;
use App\Product;
use App\Customer;
class OrdersController extends Controller
{
    /**
     * fetch order details
     * @param $id
     * @return array
     */
    public function fetchOrderData($id)
    {
        $order =  Order::where('orderNumber', '=', $id)->get();
        $order_detail = OrderDetail::where('orderNumber', '=', $id)->get();
        $this_customer = Customer::where('customerNumber', '=', $order[0]->customerNumber)->get();
        $customer[] = array('first_name' => $this_customer[0]->contactFirstName, 'last_name' => $this_customer[0]->contactLastName, 'phone' => $this_customer[0]->phone, 'country_code' => $this_customer[0]->country);
        $bill_amount = 0;
        foreach($order_detail as $item)
        {
            $bill_amount += $item->priceEach * $item->quantityOrdered;
            $product = Product::where('productCode', '=', $item->productCode)->get();
            $products[] = array('product' => $product[0]->productName, 'product_line' => $product[0]->productLine, 'unit_price' => $item->priceEach, 'qty' => $item->quantityOrdered, 'line_total' => $item->priceEach * $item->quantityOrdered);
        }
        //return $products;
       
        $data = array(
            "order_id"=>$id,
            "order_date"=>$order[0]->orderDate,
            "status"=>$order[0]->status,
            "order_details"=>$products,
            "bill_amount"=>$bill_amount,
            "customer"=>$customer[0],
        );
       return $data;
        
         
    }
}
